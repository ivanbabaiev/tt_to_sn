# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Post(models.Model):

    class Meta:
        db_table = 'post'
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    title = models.CharField(verbose_name='Title', max_length=150)  # Title
    text = models.TextField(verbose_name='Text', blank=True, null=True)  # Text
    date = models.DateTimeField(auto_now_add=True)  # Date
    author = models.ForeignKey(
        User,
        verbose_name='Author',
        on_delete=models.SET_NULL,
        related_name='posts',
        null=True
    )  # Author
    likes = models.ManyToManyField(User, verbose_name='Likes', related_name='liked_posts')  # Likes

    def __str__(self):
        return self.title

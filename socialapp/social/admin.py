# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Post


class PostAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'author', 'date', 'text')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title')


admin.site.register(Post, PostAdmin)
